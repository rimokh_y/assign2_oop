import event.Event;
import javafx.application.Application;
import javafx.stage.Stage;
import model.*;
import tek.wrapper.controller.ControllerFactory;
import userinterface.MainStageContainer;
import userinterface.WindowPosition;

import static javafx.application.Application.launch;

public class Assign2App extends Application {
    private Librarian _librarian;        // the main behavior for the application

    /**
     * Main frame of the application
     */
    private Stage mainStage;

    @Override
    public void start(Stage primaryStage) throws Exception {
        System.out.println("Library Brockport 2018");

        // Create the top-level container (main frame) and add contents to it.
        MainStageContainer.setStage(primaryStage, "Brockport Library 2018");
        mainStage = MainStageContainer.getInstance();

        mainStage.setOnCloseRequest(event -> System.exit(0));

        try {

            _librarian = ControllerFactory.get(Librarian.class);
        } catch (Exception exc) {
            System.err.println("Library - could not create Librarian!");
            new Event(Event.getLeafLevelClassName(this), "Library.<init>", "Unable to create Librarian object", Event.ERROR);
            exc.printStackTrace();
        }


        WindowPosition.placeCenter(mainStage);
        mainStage.setResizable(false);
        mainStage.show();

    }


    /**
     * The "main" entry point for the application. Carries out actions to
     * set up the application
     */
    //----------------------------------------------------------
    public static void main(String[] args) {
        launch(args);
    }
}
