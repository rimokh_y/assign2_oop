package common;

import java.util.Properties;

public class CustomProperties extends Properties {
    @Override
    public synchronized CustomProperties setProperty(String key, String value) {
        super.setProperty(key, value);
        return this;
    }

}
