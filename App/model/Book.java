// specify the package
package model;

// system imports

import exception.InvalidPrimaryKeyException;
import impresario.IView;
import tek.wrapper.controller.ControllerFactory;
import tek.wrapper.event.EventHandler;
import tek.wrapper.view.EnumViewEntry;
import userinterface.LibrarianView;

import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

import static tek.wrapper.event.EnumEventHandler.BookInsertEvent;
import static tek.wrapper.event.EnumEventHandler.UpdateStatusMessage;

// project imports

/**
 * The class containing the Account for the ATM application
 */
//==============================================================
public class Book extends EntityBase implements IView {
    private static final String myTableName = "Book";

    protected Properties dependencies;

    // GUI Components

    private String updateStatusMessage = "";

    public Book() {
        super(myTableName);
        setDependencies();
    }

    // constructor for this class
    //----------------------------------------------------------
    public Book(String bookId)
            throws InvalidPrimaryKeyException {
        super(myTableName);

        setDependencies();
        String query = "SELECT * FROM " + myTableName + " WHERE (bookId = " + bookId + ")";

        Vector<Properties> allDataRetrieved = getSelectQueryResult(query);

        // You must get one account at least
        if (allDataRetrieved != null) {
            int size = allDataRetrieved.size();

            // There should be EXACTLY one account. More than that is an error
            if (size > 1) {
                throw new InvalidPrimaryKeyException("Multiple accounts matching id : "
                        + bookId + " found.");
            } else if (size == 0) {
                throw new InvalidPrimaryKeyException("No account matching id : "
                        + bookId + " found.");
            } else {
                // copy all the retrieved data into persistent state
                Properties retrievedAccountData = allDataRetrieved.elementAt(0);
                persistentState = new Properties();

                Enumeration allKeys = retrievedAccountData.propertyNames();
                while (allKeys.hasMoreElements()) {
                    String nextKey = (String) allKeys.nextElement();
                    String nextValue = retrievedAccountData.getProperty(nextKey);
                    // accountNumber = Integer.parseInt(retrievedAccountData.getProperty("accountNumber"));

                    if (nextValue != null) {
                        persistentState.setProperty(nextKey, nextValue);
                    }
                }

            }
        }
        // If no account found for this user name, throw an exception
        else {
            throw new InvalidPrimaryKeyException("No book matching id : "
                    + bookId + " found.");
        }
    }

    // Can also be used to create a NEW Account (if the system it is part of
    // allows for a new account to be set up)
    //----------------------------------------------------------
    public Book(Properties props) {
        super(myTableName);

        setDependencies();
        fromProperties(props);
    }

    //-----------------------------------------------------------------------------------
    private void setDependencies() {
        dependencies = new Properties();

        myRegistry.setDependencies(dependencies);
        _eventHandler.register(BookInsertEvent, (params) -> {
            fromProperties((Properties) params);

            this.update();
            this.createView(EnumViewEntry.BookInsertView);
            this.updateState(UpdateStatusMessage.name(), this);
        });
    }

    public String bookId() {
        return (String) getState("bookId");
    }

    public String author() {
        return (String) getState("author");
    }

    public Book author(String value) {
        persistentState.setProperty("author", value);
        return this;
    }

    public String title() {
        return (String) getState("title");
    }

    public Book title(String value) {
        persistentState.setProperty("title", value);
        return this;
    }

    public String pubYear() {
        return (String) getState("pubYear");
    }

    public Book pubYear(String value) {
        persistentState.setProperty("pubYear", value);
        return this;
    }

    public String status() {
        return (String) getState("status");
    }

    public Book status(String value) {
        persistentState.setProperty("status", value);
        return this;
    }

    //----------------------------------------------------------
    public Object getState(String key) {
        if (key.equals(UpdateStatusMessage.name())) {
            return updateStatusMessage;
        }
        return persistentState.getProperty(key);
    }

    public void updateState(String key, Object value) {
        stateChangeRequest(key, value);
    }

    public void update() {
        updateStateInDatabase();
    }

    private void updateStateInDatabase() {
        try {
            if (persistentState.getProperty("bookId") != null) {
                Properties whereClause = new Properties();
                whereClause.setProperty("bookId",
                        persistentState.getProperty("bookId"));
                updatePersistentState(mySchema, persistentState, whereClause);
                updateStatusMessage = "Book data for book id: " + persistentState.getProperty("bookId") + " updated successfully in database!";
            } else {
                Integer bookId =
                        insertAutoIncrementalPersistentState(mySchema, persistentState);
                persistentState.setProperty("bookId", "" + bookId);
                updateStatusMessage = "Book data for new book : " + persistentState.getProperty("bookId")
                        + " installed successfully in database!";
            }
        } catch (SQLException ex) {
            updateStatusMessage = "Error in installing account data in database!";
        }
    }

    //-----------------------------------------------------------------------------------
    protected void initializeSchema(String tableName) {
        if (mySchema == null) {
            mySchema = getSchemaInfo(tableName);
        }
    }
}