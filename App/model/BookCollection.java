package model;


import exception.NoResultException;
import impresario.IView;
import tek.wrapper.controller.ControllerFactory;
import tek.wrapper.event.EnumEventHandler;
import tek.wrapper.event.EventHandler;
import userinterface.BookModel;
import tek.wrapper.view.EnumViewEntry;

import java.time.Year;
import java.util.Properties;
import java.util.Vector;
import java.util.stream.Collectors;

import static tek.wrapper.event.EnumEventHandler.BookSearchEvent;
import static tek.wrapper.event.EnumEventHandler.BookSearchResult;

public class BookCollection extends EntityBase implements IView {
    private static final String myTableName = "Book";

    private Vector<BookModel> books;

    private Properties dependencies;

    public BookCollection() {
        super(myTableName);

        books = new Vector<>();
        setDependencies();
    }

    private void setDependencies() {
        dependencies = new Properties();

        myRegistry.setDependencies(dependencies);
        _eventHandler.register(BookSearchEvent, (params) -> {
            try {
                findBooksWithTitleLike((String) params);
            } catch (NoResultException e) {
                System.err.println("[BookCollection]: " + e.getMessage());
            }
            createView(EnumViewEntry.BookSearchView);
            updateState(EnumEventHandler.BookSearchResult.name(), this);
        });
    }

    public Vector<BookModel> findBooksOlderThanDate(final String yearStr) throws NoResultException {
        String query = "SELECT * FROM " + myTableName + " WHERE pubYear is not NULL";
        Year pubYear = Year.parse(yearStr);
        Vector<Properties> properties = getSelectQueryResult(query);

        if (properties == null) {
            throw new NoResultException("No books found");
        }
        return books = properties.stream()
                .filter((e) -> {
                    Year entityYear = Year.parse(e.getProperty("pubYear")); // Parses pubYear

                    System.out.println(pubYear + " isAfter " + entityYear + " = " + pubYear.isAfter(entityYear));
                    return pubYear.isAfter(entityYear); // Is the entity pubYear older ?
                })
                .map(BookModel::new) // Calls constructor with the Properties instance
                .collect(Collectors.toCollection(Vector::new)); // Store instance of Books in the vector 'books'
    }

    public Vector<BookModel> findBooksNewerThanDate(final String yearStr) throws NoResultException {
        String query = "SELECT * FROM " + myTableName + " WHERE pubYear is not NULL";
        Year pubYear = Year.parse(yearStr);
        Vector<Properties> properties = getSelectQueryResult(query);

        if (properties == null) {
            throw new NoResultException("No books found");
        }
        return books = properties.stream()
                .filter((e) -> {
                    Year entityYear = Year.parse(e.getProperty("pubYear")); // Parses pubYear

                    return pubYear.isBefore(entityYear); // Is the entity pubYear newer ?
                })
                .map(BookModel::new) // Calls Book constructor with the Properties instance
                .collect(Collectors.toCollection(Vector::new)); // Store instance of Books in the vector 'books'
    }

    public Vector<BookModel> findBooksWithTitleLike(final String titlePattern) throws NoResultException {
        String query = "SELECT * FROM " + myTableName + " WHERE title LIKE '%" + titlePattern + "%'";
        Vector<Properties> properties = getSelectQueryResult(query);

        if (properties == null) {
            throw new NoResultException("No author match found for pattern '" + titlePattern + "'");
        }
        return books = properties.stream()
                .map(BookModel::new) // Calls Book constructor with the Properties instance
                .collect(Collectors.toCollection(Vector::new)); // Store instance of Books in the vector 'books';
    }

    public Vector<BookModel> findBooksWithAuthorLike(final String authorPattern) throws NoResultException {
        String query = "SELECT * FROM " + myTableName + " WHERE author LIKE '%" + authorPattern + "%'";
        System.out.println(query);
        Vector<Properties> properties = getSelectQueryResult(query);

        if (properties == null) {
            throw new NoResultException("No author match found for pattern '" + authorPattern + "'");
        }
        return books = properties.stream()
                .map(BookModel::new) // Calls Book constructor with the Properties instance
                .collect(Collectors.toCollection(Vector::new)); // Store instance of Books in the vector 'books';
    }

    public Object getState(String key) {
        if (key.equals(BookSearchResult.name())) {
            return books;
        }
        return persistentState.getProperty(key);
    }

    protected void initializeSchema(String tableName) {
        if (mySchema == null) {
            mySchema = getSchemaInfo(tableName);
        }
    }

    public Vector<BookModel> getBooks() {
        return books;
    }

    @Override
    public void updateState(String key, Object value) {
        stateChangeRequest(key, value);
    }
}