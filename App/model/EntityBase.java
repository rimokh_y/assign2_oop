// tabs=4
//************************************************************
//	COPYRIGHT 2009/2015 Sandeep Mitra, Michael Steves and T M Rao, The
//    College at Brockport, State University of New York. -
//	  ALL RIGHTS RESERVED
//
// This file is the product of The College at Brockport and cannot
// be reproduced, copied, or used in any shape or form without
// the express written consent of The College at Brockport.
//************************************************************
//
// specify the package
package model;

// system imports

import database.Persistable;
import event.Event;
import impresario.IModel;
import impresario.IView;
import impresario.ModelRegistry;
import javafx.scene.Scene;
import javafx.stage.Stage;
import tek.wrapper.event.EnumEventHandler;
import tek.wrapper.event.EventHandler;
import tek.wrapper.view.EnumViewEntry;
import userinterface.*;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.function.Consumer;

// project imports


/**
 * The superclass for all Fast Trax Model Entities that are also
 * Persistable
 */
//==============================================================
public abstract class EntityBase extends Persistable
        implements IModel {
    protected ModelRegistry myRegistry;    // registry for entities interested in our events
    private int referenceCount;        // the number of others using us
    protected boolean dirty;        // true if the data has changed
    protected Properties persistentState;    // the field names and values from the database
    private String myTableName;                // the name of our database table

    protected Hashtable<String, Scene> myViews;
    protected Stage myStage;
    protected EnumViewEntry _currentView = EnumViewEntry.LibrarianView;

    protected Properties mySchema;
    protected EventHandler _eventHandler = new EventHandler<>(this);

    // forward declarations
    public abstract Object getState(String key);

    public void stateChangeRequest(String key, Object value) {
        _eventHandler.dispatch(key, value);
        myRegistry.updateSubscribers(key, this);
    }

    protected abstract void initializeSchema(String tableName);

    // constructor for this class
    //----------------------------------------------------------
    protected EntityBase(String tablename) {
        myStage = MainStageContainer.getInstance();
        myViews = new Hashtable<>();

        // save our table name for later
        myTableName = tablename;

        // extract the schema from the database, calls methods in subclasses
        initializeSchema(myTableName);

        // create a place to hold our state from the database
        persistentState = new Properties();

        // create a registry for subscribers
        myRegistry = new ModelRegistry("EntityBase." + tablename);    // for now

        // initialize the reference count
        referenceCount = 0;
        // indicate the data in persistentState matches the database contents
        dirty = false;
    }

    @Override
    public void subscribe(String key, IView subscriber) {
        // DEBUG: System.out.println("EntityBase[" + myTableName + "].subscribe");
        // forward to our registry
        myRegistry.subscribe(key, subscriber);
    }

    @Override
    public void unSubscribe(String key, IView subscriber) {
        // DEBUG: System.out.println("EntityBase.unSubscribe");
        // forward to our registry
        myRegistry.unSubscribe(key, subscriber);
    }


    public void createView(EnumViewEntry viewEntry) {
        Scene currentScene = myViews.get(viewEntry.name());

        if (currentScene == null) {
            View newView = ViewFactory.createView(viewEntry.name(), this);
            currentScene = new Scene(newView);
            myViews.put(viewEntry.name(), currentScene);
        }
        swapToView(currentScene);
        myViews.remove(_currentView.name());
        _currentView = viewEntry;
    }

    private void swapToView(Scene newScene) {
        if (newScene == null) {
            new Event(Event.getLeafLevelClassName(this), "swapToView",
                    "Missing view for display ", Event.ERROR);
            return;
        }

        myStage.setScene(newScene);
        myStage.sizeToScene();

        WindowPosition.placeCenter(myStage);
    }

    protected void fromProperties(Properties props) {
        persistentState = new Properties();
        Enumeration allKeys = props.propertyNames();
        while (allKeys.hasMoreElements()) {
            String nextKey = (String) allKeys.nextElement();
            String nextValue = props.getProperty(nextKey);

            if (nextValue != null) {
                persistentState.setProperty(nextKey, nextValue);
            }
        }
    }

}

