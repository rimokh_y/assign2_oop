package model;


import impresario.IView;
import javafx.scene.Scene;
import javafx.stage.Stage;
import tek.wrapper.controller.ControllerFactory;
import tek.wrapper.event.EventHandler;
import tek.wrapper.view.EnumViewEntry;
import userinterface.MainStageContainer;

import java.util.Hashtable;
import java.util.Properties;

// project imports

public class Librarian extends EntityBase implements IView {
    // For Impresario
    private Properties dependencies;

    public Librarian() {
        super("Librarian");
        setDependencies();
        createView(_currentView);
    }

    private void setDependencies() {
        dependencies = new Properties();

        myRegistry.setDependencies(dependencies);
    }

    public Object getState(String key) {
        return "";
    }

    @Override
    public void stateChangeRequest(String key, Object value) {
        System.out.println("[LibrarianController]: " + key);

        ControllerFactory.dispatchEvent(key, value);
        _eventHandler.dispatch(key, value);
        myRegistry.updateSubscribers(key, this);
    }

    @Override
    protected void initializeSchema(String tableName) {
    }

    public void updateState(String key, Object value) {
        stateChangeRequest(key, value);
    }

    public void subscribe(String key, IView subscriber) {
        myRegistry.subscribe(key, subscriber);
    }

    public void unSubscribe(String key, IView subscriber) {
        myRegistry.unSubscribe(key, subscriber);
    }
}

