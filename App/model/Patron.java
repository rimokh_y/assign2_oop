// specify the package
package model;

// system imports

import exception.InvalidPrimaryKeyException;
import tek.wrapper.controller.ControllerFactory;
import tek.wrapper.event.EnumEventHandler;
import tek.wrapper.event.EventHandler;
import tek.wrapper.view.EnumViewEntry;

import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

import static tek.wrapper.event.EnumEventHandler.PatronInsertEvent;
import static tek.wrapper.event.EnumEventHandler.UpdateStatusMessage;

// project imports

/**
 * The class containing the Account for the ATM application
 */
//==============================================================
public class Patron extends EntityBase {
    private static final String myTableName = "Patron";

    protected Properties dependencies;

    // GUI Components

    private String updateStatusMessage = "";

    public Patron() {
        super(myTableName);
        setDependencies();
    }

    public Patron(String patronId)
            throws InvalidPrimaryKeyException {
        super(myTableName);

        setDependencies();
        String query = "SELECT * FROM " + myTableName + " WHERE (patronId = " + patronId + ")";

        Vector<Properties> allDataRetrieved = getSelectQueryResult(query);

        // You must get one account at least
        if (allDataRetrieved != null) {
            int size = allDataRetrieved.size();

            // There should be EXACTLY one account. More than that is an error
            if (size > 1) {
                throw new InvalidPrimaryKeyException("Multiple accounts matching id : "
                        + patronId + " found.");
            } else if (size == 0) {
                throw new InvalidPrimaryKeyException("No account matching id : "
                        + patronId + " found.");
            } else {
                // copy all the retrieved data into persistent state
                Properties retrievedAccountData = allDataRetrieved.elementAt(0);
                persistentState = new Properties();

                Enumeration allKeys = retrievedAccountData.propertyNames();
                while (allKeys.hasMoreElements()) {
                    String nextKey = (String) allKeys.nextElement();
                    String nextValue = retrievedAccountData.getProperty(nextKey);
                    // accountNumber = Integer.parseInt(retrievedAccountData.getProperty("accountNumber"));

                    if (nextValue != null) {
                        persistentState.setProperty(nextKey, nextValue);
                    }
                }

            }
        }
        // If no account found for this user name, throw an exception
        else {
            throw new InvalidPrimaryKeyException("No book matching id : "
                    + patronId + " found.");
        }
    }

    // Can also be used to create a NEW Account (if the system it is part of
    // allows for a new account to be set up)
    //----------------------------------------------------------
    public Patron(Properties props) {
        super(myTableName);

        setDependencies();
        fromProperties(props);
    }

    //-----------------------------------------------------------------------------------
    private void setDependencies() {
        dependencies = new Properties();

        myRegistry.setDependencies(dependencies);
        _eventHandler.register(PatronInsertEvent, (params) -> {
            fromProperties((Properties) params);

            this.update();
            createView(EnumViewEntry.PatronInsertView);
            updateState(UpdateStatusMessage.name(), this);
        });
    }

    //----------------------------------------------------------
    public Object getState(String key) {
        if (key.equals(UpdateStatusMessage.name()))
            return updateStatusMessage;

        return persistentState.getProperty(key);
    }
    
    /**
     * Called via the IView relationship
     */
    //----------------------------------------------------------
    public void updateState(String key, Object value) {
        stateChangeRequest(key, value);
    }

    //-----------------------------------------------------------------------------------
    public void update() {
        updateStateInDatabase();
    }

    //-----------------------------------------------------------------------------------
    private void updateStateInDatabase() {
        try {
            if (persistentState.getProperty("patronId") != null) {
                Properties whereClause = new Properties();
                whereClause.setProperty("patronId",
                        persistentState.getProperty("patronId"));
                updatePersistentState(mySchema, persistentState, whereClause);
                updateStatusMessage = "Patron data for patron id: " + persistentState.getProperty("patronId") + " updated successfully in database!";
            } else {
                Integer patronId =
                        insertAutoIncrementalPersistentState(mySchema, persistentState);
                persistentState.setProperty("patronId", "" + patronId);
                updateStatusMessage = "Patron for new patron: " + persistentState.getProperty("patronId")
                        + "installed successfully in database!";
            }
        } catch (SQLException ex) {
            updateStatusMessage = "Error in installing patron data in database!";
        }
        System.out.println("updateStateInDatabase " + updateStatusMessage);
    }

    //-----------------------------------------------------------------------------------
    protected void initializeSchema(String tableName) {
        if (mySchema == null) {
            mySchema = getSchemaInfo(tableName);
        }
    }
}

