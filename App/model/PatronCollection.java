package model;


import exception.NoResultException;
import tek.wrapper.controller.ControllerFactory;
import tek.wrapper.event.EnumEventHandler;
import tek.wrapper.event.EventHandler;
import tek.wrapper.view.EnumViewEntry;
import userinterface.PatronModel;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import java.util.Vector;
import java.util.stream.Collectors;

import static tek.wrapper.event.EnumEventHandler.PatronSearchEvent;
import static tek.wrapper.event.EnumEventHandler.PatronSearchResult;
import static tek.wrapper.event.EnumEventHandler.UpdateStatusMessage;

public class PatronCollection extends EntityBase {
    private static final String myTableName = "Patron";

    protected Properties dependencies;
    private Vector<PatronModel> patronList;

    private String updateStatusMessage = "";

    public PatronCollection() {
        super(myTableName);
        setDependencies();

        patronList = new Vector<>();

    }

    private void setDependencies() {
        dependencies = new Properties();

        _eventHandler.register(PatronSearchEvent, (params) -> {
            try {
                findPatronsAtZipCode((String) params);
            } catch (NoResultException e) {
                System.err.println("[PatronResult]: " + e.getMessage());
            }
            createView(EnumViewEntry.PatronSearchView);
            updateState(PatronSearchResult.name(), this);//TODO: Use system to send patronresult
        });
        myRegistry.setDependencies(dependencies);
    }

    public Vector<PatronModel> findPatronsOlderThan(String date) throws NoResultException {
        String query = "SELECT * FROM " + myTableName + " WHERE dateOfBirth is not NULL";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate birthdayDate = LocalDate.parse(date, formatter);
        Vector<Properties> properties = getSelectQueryResult(query);

        if (properties == null) {
            throw new NoResultException("No patrons found");
        }
        return patronList = properties.stream()
                .filter(e -> birthdayDate.isBefore(LocalDate.parse(e.getProperty("dateOfBirth"), formatter)))
                .map(PatronModel::new)
                .collect(Collectors.toCollection(Vector::new));
    }

    public Vector<PatronModel> findPatronsYoungerThan(String date) throws NoResultException {
        String query = "SELECT * FROM " + myTableName + " WHERE dateOfBirth is not NULL";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate birthdayDate = LocalDate.parse(date, formatter);
        Vector<Properties> properties = getSelectQueryResult(query);

        if (properties == null) {
            throw new NoResultException("No patrons found");
        }
        return patronList = properties.stream()
                .filter(e -> birthdayDate.isAfter(LocalDate.parse(e.getProperty("dateOfBirth"), formatter)))
                .map(PatronModel::new)
                .collect(Collectors.toCollection(Vector::new));
    }

    public Vector<PatronModel> findPatronsAtZipCode(String zip) throws NoResultException {
        String query = "SELECT * FROM " + myTableName + " WHERE zip = '" + zip + "'";
        Vector<Properties> properties = getSelectQueryResult(query);

        if (properties == null) {
            throw new NoResultException("No patron match found for pattern '" + zip + "'");
        }
        return patronList = properties.stream()
                .map(PatronModel::new)
                .collect(Collectors.toCollection(Vector::new));
    }

    public Vector<PatronModel> findPatronsWithNameLike(String name) throws NoResultException {
        String query = "SELECT * FROM " + myTableName + " WHERE name LIKE '%" + name + "%'";
        Vector<Properties> properties = getSelectQueryResult(query);

        if (properties == null) {
            throw new NoResultException("No patron match found for pattern '" + name + "'");
        }
        return patronList = properties.stream()
                .map(PatronModel::new)
                .collect(Collectors.toCollection(Vector::new));
    }

    public Object getState(String key) {
        if (key.equals(PatronSearchResult.name())) {
            return patronList;
        }
        return persistentState.getProperty(key);
    }

    public void updateState(String key, Object value) {
        stateChangeRequest(key, value);
    }

    public void update() {
        updateStateInDatabase();
    }

    private void updateStateInDatabase() {
        try {
            if (persistentState.getProperty("patronId") != null) {
                Properties whereClause = new Properties();
                whereClause.setProperty("patronId",
                        persistentState.getProperty("patronId"));
                updatePersistentState(mySchema, persistentState, whereClause);
                updateStatusMessage = "Patron data for patron id: " + persistentState.getProperty("patronId") + " updated successfully in database!";
            } else {
                Integer patronId =
                        insertAutoIncrementalPersistentState(mySchema, persistentState);
                persistentState.setProperty("patronId", String.valueOf(patronId.intValue()));
                updateStatusMessage = "Patron data for new patron : " + persistentState.getProperty("patronId")
                        + "installed successfully in database!";
            }
        } catch (SQLException ex) {
            updateStatusMessage = "Error in installing patron data in database!";

        }
        //DEBUG System.out.println("updateStateInDatabase " + updateStatusMessage);
    }

    protected void initializeSchema(String tableName) {
        if (mySchema == null) {
            mySchema = getSchemaInfo(tableName);
        }
    }

    public Vector<PatronModel> getPatronList() {
        return patronList;
    }
}