package tek.wrapper.controller;

import impresario.IModel;
import model.*;
import tek.wrapper.event.EnumEventHandler;
import tek.wrapper.view.EnumViewEntry;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ControllerFactory {
    private static Map<Class<? extends IModel>, IModel> controllers = new HashMap<>();

    static {
        controllers.put(Librarian.class, new Librarian());
        controllers.put(Book.class, new Book());
        controllers.put(BookCollection.class, new BookCollection());
        controllers.put(Patron.class, new Patron());
        controllers.put(PatronCollection.class, new PatronCollection());
    }

    public static <T extends IModel> T get(Class<?> modelClass) throws IllegalArgumentException {
        return Optional.ofNullable((T) controllers.get(modelClass)).orElseThrow(() -> new IllegalArgumentException("No controller found of type [" + modelClass.getName() + "]"));
    }

    public static void dispatchEvent(EnumEventHandler event, Object params) {
        IModel model = event.controller();

        model.stateChangeRequest(event.name(), params);
//        System.out.println("[" + model.getClass().getName() + "]: " + "Updated with key " + event.name());
    }

    public static void dispatchEvent(String eventKey, Object params) {
        try {
            EnumEventHandler event = EnumEventHandler.valueOf(eventKey);

            dispatchEvent(event, params);
        } catch (IllegalArgumentException e) {
            System.err.println("[" + eventKey + "]: " + "Didnt find any receiver");
            //TODO: DEBUG MODE
        }
    }
}
