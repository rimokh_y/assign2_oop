package tek.wrapper.event;

import impresario.IModel;
import model.*;
import tek.wrapper.controller.ControllerFactory;

public enum EnumEventHandler {
    BookInsertEvent(Book.class),
    BookSearchEvent(BookCollection.class),
    PatronSearchEvent(PatronCollection.class),
    PatronInsertEvent(Patron.class),
    BookSearchResult(null),
    PatronSearchResult(null),
    UpdateStatusMessage(null);

    private Class<? extends EntityBase> _controllerClass;

    EnumEventHandler(Class<? extends EntityBase> controllerClass) {
        _controllerClass = controllerClass;
    }

    public IModel controller() {
        return ControllerFactory.get(_controllerClass);
    }
}
