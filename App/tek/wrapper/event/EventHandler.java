package tek.wrapper.event;

import model.EntityBase;
import tek.wrapper.view.EnumViewEntry;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

public class EventHandler<T extends EntityBase> {
    private Map<EnumEventHandler, Consumer<Object>> _events = new HashMap<>();
    private T _controller;
    private String loggingPrefix;

    public EventHandler(T entity) {
        _controller = entity;
        loggingPrefix = "[" + entity.getClass().getName() + "]";
    }

    public EventHandler register(EnumEventHandler event, Consumer<Object> action) {
        _events.put(event, action);
        return this;
    }

    public Consumer<Object> get(EnumEventHandler event) {
        return Optional.ofNullable(_events.get(event))
                .orElseThrow(() -> new IllegalArgumentException("No event with name [" + event.name() + "]"));
    }

    public void dispatch(EnumEventHandler event, Object params) {
        get(event).accept(params);
    }

    public void eventViewChange(EnumViewEntry entry) {
        if (entry.controller().equals(_controller)) {
            _controller.createView(entry);
        } else {
            entry.controller().stateChangeRequest(entry.name(), null);
        }
    }

    public void eventViewChange(String viewKey) {
        try {
            eventViewChange(EnumViewEntry.valueOf(viewKey));
            } catch (IllegalArgumentException e) {
//            System.err.println(loggingPrefix + ": " + "No view found for ViewEntry[" + viewKey + "]");
            //TODO: DEBUG MODE
        }
    }

    public void dispatch(String eventKey, Object params) {
        try {
            EnumEventHandler event = EnumEventHandler.valueOf(eventKey);

            dispatch(event, params);
        } catch (IllegalArgumentException ignored) {
            eventViewChange(eventKey);
//            System.err.println(loggingPrefix + ": " + "No event action found for [" + eventKey + "]");
            //TODO: DEBUG MODE

        }
    }
}
