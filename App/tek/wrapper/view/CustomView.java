// tabs=4
//************************************************************
//	COPYRIGHT 2009/2015 Sandeep Mitra and Michael Steves, The
//    College at Brockport, State University of New York. - 
//	  ALL RIGHTS RESERVED
//
// This file is the product of The College at Brockport and cannot 
// be reproduced, copied, or used in any shape or form without 
// the express written consent of The College at Brockport.
//************************************************************
//
// specify the package
package tek.wrapper.view;

// system imports

import impresario.ControlRegistry;
import impresario.IControl;
import impresario.IModel;
import impresario.IView;
import javafx.scene.Group;
import model.EntityBase;
import tek.wrapper.event.EnumEventHandler;
import tek.wrapper.event.EventHandler;
import userinterface.View;

import java.util.function.Consumer;

// project imports

//==============================================================
public abstract class CustomView extends View {
    // private data
    protected IModel myModel;
    protected ControlRegistry myRegistry;
    protected EventHandler _eventHandler;

    // GUI components


    // Class constructor
    //----------------------------------------------------------
    public CustomView(IModel model, String classname) {
        super(model, classname);
        myModel = model;
        myRegistry = new ControlRegistry(classname);
        _eventHandler = new EventHandler<>((EntityBase) myModel);
    }

    //---------------------1-------------------------------------
    public void setRegistry(ControlRegistry registry) {
        myRegistry = registry;
    }

    // Allow models to register for state updates
    //----------------------------------------------------------
    public void subscribe(String key, IModel subscriber) {
        myRegistry.subscribe(key, subscriber);
    }

    // Allow models to register for state updates
    //----------------------------------------------------------
    public void subscribe(String key, IModel subscriber, Consumer<Object> action) {
        try {
            _eventHandler.register(EnumEventHandler.valueOf(key), action);
            myRegistry.subscribe(key, subscriber);
        } catch (IllegalArgumentException e) {
            System.err.println("Not EnumEventHandler found for key [" + key + "]");
        }
    }


    // Allow models to unregister for state updates
    //----------------------------------------------------------
    public void unSubscribe(String key, IModel subscriber) {
        myRegistry.unSubscribe(key, subscriber);
    }

    public void updateState(String key, Object value) {
        _eventHandler.dispatch(key, value);
    }
}

