package tek.wrapper.view;

import impresario.IModel;
import model.*;
import tek.wrapper.controller.ControllerFactory;
import userinterface.*;

import java.lang.reflect.Constructor;

public enum EnumViewEntry {
    LibrarianView(LibrarianView.class, Librarian.class),
    BookInsertView(BookInsertView.class, Book.class),
    BookSearchView(BookSearchView.class, BookCollection.class),
    PatronInsertView(PatronInsertView.class, Patron.class),
    PatronSearchView(PatronSearchView.class, PatronCollection.class);

    private final Class<? extends IModel> _controller;
    private Class<? extends View> _viewClass;

    EnumViewEntry(Class<? extends View> viewClass, Class<? extends IModel> controller) {
        _viewClass = viewClass;
        _controller = controller;
    }

    public View view(IModel model) {
        try {
            Constructor viewCtor = _viewClass.getConstructor(IModel.class);

            return (View) viewCtor.newInstance(model);
        } catch (Exception e) {
            e.printStackTrace();//TODO: throw explicit exception
            return null;
        }
    }

    public <T extends IModel> T controller() {
        return ControllerFactory.get(_controller);
    }
}