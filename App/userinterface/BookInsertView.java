// specify the package
package userinterface;

// system imports

import common.CustomProperties;
import common.StringUtils;
import impresario.IModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import model.EntityBase;
import tek.wrapper.event.EventHandler;
import tek.wrapper.view.EnumViewEntry;

import java.time.Year;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

import static tek.wrapper.event.EnumEventHandler.*;

// project imports

/**
 * The class containing the Account View  for the ATM application
 */
//==============================================================
public class BookInsertView extends View {
    private static ObservableList STATUS_SELECTION = FXCollections.observableArrayList("Active", "Inactive");

    private TextField _author = new TextField();
    private TextField _title = new TextField();
    private TextField _pubYear = new TextField();
    private ComboBox _status = new ComboBox(STATUS_SELECTION);

    private Button _done = new Button("DONE");
    private Button _submit = new Button("SUBMIT");

    // For showing error message
    private MessageView statusLog;

    // constructor for this class -- takes a model object
    //----------------------------------------------------------
    public BookInsertView(IModel account) {
        super(account, EnumViewEntry.BookInsertView.name());

        VBox container = new VBox(10);
        container.setPadding(new Insets(15, 5, 5, 5));
        container.getChildren().add(createTitle());
        container.getChildren().add(createFormContent());
        container.getChildren().add(createStatusLog("             "));
        getChildren().add(container);

        populateFields();
        myModel.subscribe(UpdateStatusMessage.name(), this);
        _eventHandler.register(UpdateStatusMessage, this::setStatusMsg);
    }

    public void setStatusMsg(Object params) {
        String msg = (String) params;

        System.out.println("[BookInsertView] Complete update msg: " + msg);
        if (msg.toLowerCase().contains("error")) {
            displayErrorMessage("Insertion failure");
        } else {
            displayMessage("Insertion success");
        }
    }

    // Create the title container
    //-------------------------------------------------------------
    private Node createTitle() {
        HBox container = new HBox();
        container.setAlignment(Pos.CENTER);

        Text titleText = new Text("            Insert New Book            ");
        titleText.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        titleText.setWrappingWidth(300);
        titleText.setTextAlignment(TextAlignment.CENTER);
        titleText.setFill(Color.DARKGREEN);
        container.getChildren().add(titleText);

        return container;
    }


    // Create the main form content
    //-------------------------------------------------------------
    private VBox createFormContent() {
        VBox vbox = new VBox(10);
        GridPane grid = new GridPane();

        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        grid.add(new Label("Author:"), 0, 1);
        grid.add(_author, 1, 1);

        grid.add(new Label("Title:"), 0, 2);
        grid.add(_title, 1, 2);

        grid.add(new Label("Publication year:"), 0, 3);
        grid.add(_pubYear, 1, 3);

        _status.getSelectionModel().selectFirst();
        grid.add(new Label("Active :"), 0, 4);
        grid.add(_status, 1, 4);

        _done.setOnAction(this::processDone);
        _submit.setOnAction(this::processSubmit);
        grid.add(_done, 0, 5);
        grid.add(_submit, 1, 5);
        vbox.getChildren().add(grid);

        return vbox;
    }

    private boolean validateYearOffset(Year year) {
        Year min = Year.parse("1800");

        return year.equals(Year.now()) || year.equals(min) ||
                (year.isAfter(min) && year.isBefore(Year.now()));
    }

    private List<String> generateErrorMessage() {
        List<String> errors = new ArrayList<>();

        if (StringUtils.isNullOrEmpty(_author.getText())) {
            errors.add("Author: Cannot be empty");
        }
        if (StringUtils.isNullOrEmpty(_title.getText())) {
            errors.add("Title: cannot be empty");
        }
        if (StringUtils.isNullOrEmpty(_pubYear.getText())) {
            errors.add("Publication year: cannot be empty");
        } else {
            try {
                Year year = Year.parse(_pubYear.getText());

                if (!validateYearOffset(year)) {
                    errors.add("Publication year: Between 1800 and " + Year.now());
                }
            } catch (DateTimeParseException e) {
                errors.add("Publication year: Invalid date format");
            }
        }
        return errors;
    }

    private void processSubmit(ActionEvent actionEvent) {
        clearErrorMessage();
        List<String> errors = generateErrorMessage();

        if (errors.isEmpty()) {
            myModel.stateChangeRequest("BookInsertEvent", new CustomProperties()
                    .setProperty("author", _author.getText())
                    .setProperty("title", _title.getText())
                    .setProperty("pubYear", _pubYear.getText())
                    .setProperty("status", (String) _status.getSelectionModel().getSelectedItem())
            );
        } else {
            displayErrorMessage(errors.get(0));
        }
    }

    private void processDone(ActionEvent actionEvent) {
        myModel.stateChangeRequest(EnumViewEntry.LibrarianView.name(), null);
    }

    private MessageView createStatusLog(String initialMessage) {
        statusLog = new MessageView(initialMessage);

        statusLog.maxWidth(MainStageContainer.getInstance().getMaxHeight());
        return statusLog;
    }

    private void populateFields() {
        _author.setText("");
        _title.setText("");
        _pubYear.setText("");
        clearErrorMessage();
    }

    private void displayErrorMessage(String message) {
        statusLog.displayErrorMessage(message);
    }

    private void displayMessage(String message) {
        statusLog.displayMessage(message);
    }

    private void clearErrorMessage() {
        statusLog.clearErrorMessage();
    }

}
