package userinterface;

import javafx.beans.property.SimpleStringProperty;
import model.Book;

import java.util.Properties;

public class BookModel {
    private final SimpleStringProperty bookId;
    private final SimpleStringProperty title;
    private final SimpleStringProperty author;
    private final SimpleStringProperty pubYear;
    private final SimpleStringProperty status;

    public BookModel(Book book) {
        this.bookId = new SimpleStringProperty(book.bookId());
        this.title = new SimpleStringProperty(book.title());
        this.author = new SimpleStringProperty(book.author());
        this.pubYear = new SimpleStringProperty(book.pubYear());
        this.status = new SimpleStringProperty(book.status());
    }

    public BookModel(Properties book) {
        this.bookId = new SimpleStringProperty(book.getProperty("bookId"));
        this.title = new SimpleStringProperty(book.getProperty("title"));
        this.author = new SimpleStringProperty(book.getProperty("author"));
        this.pubYear = new SimpleStringProperty(book.getProperty("pubYear"));
        this.status = new SimpleStringProperty(book.getProperty("status"));
    }

    public String getBookId() {
        return bookId.get();
    }

    public SimpleStringProperty bookIdProperty() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId.set(bookId);
    }

    public String getTitle() {
        return title.get();
    }

    public SimpleStringProperty titleProperty() {
        return title;
    }

    public void setTitle(String title) {
        this.title.set(title);
    }

    public String getAuthor() {
        return author.get();
    }

    public SimpleStringProperty authorProperty() {
        return author;
    }

    public void setAuthor(String author) {
        this.author.set(author);
    }

    public String getPubYear() {
        return pubYear.get();
    }

    public SimpleStringProperty pubYearProperty() {
        return pubYear;
    }

    public void setPubYear(String pubYear) {
        this.pubYear.set(pubYear);
    }

    public String getStatus() {
        return status.get();
    }

    public SimpleStringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }
}
