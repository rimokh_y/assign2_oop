// specify the package
package userinterface;

// system imports

import common.StringUtils;
import impresario.IModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import model.Book;
import model.EntityBase;
import tek.wrapper.event.EnumEventHandler;
import tek.wrapper.event.EventHandler;
import tek.wrapper.view.EnumViewEntry;

import java.util.List;

// project imports

/**
 * The class containing the Account View  for the ATM application
 */
//==============================================================
public class BookSearchView extends View {
    private static ObservableList<TableColumn> DATATABLE_COLUMNS = FXCollections.observableArrayList(
            new TableColumn<>("BookId"),
            new TableColumn<>("Author"),
            new TableColumn<>("Title"),
            new TableColumn<>("Publication Year"),
            new TableColumn<>("Status")
    );

    private TableView _datatable = new TableView();
    private TextField _titlePattern = new TextField();
    private Button _done = new Button("DONE");
    private Button _submit = new Button("SUBMIT");

    private MessageView statusLog;

    static {
        DATATABLE_COLUMNS.forEach(tableColumn -> {
            String prop = "";
            switch (tableColumn.getText()) {
                case "BookId":
                    prop = "bookId";
                    tableColumn.setPrefWidth(50);
                    break;
                case "Author":
                    tableColumn.setPrefWidth(150);
                    prop = "author";
                    break;
                case "Title":
                    tableColumn.setPrefWidth(150);
                    prop = "title";
                    break;
                case "Publication Year":
                    tableColumn.setPrefWidth(150);
                    prop = "pubYear";
                    break;
                case "Status":
                    tableColumn.setPrefWidth(150);
                    prop = "status";
                    break;
            }
            tableColumn.setPrefWidth(150);
            tableColumn.setCellValueFactory(new PropertyValueFactory<>(prop));
        });
    }

    public BookSearchView(IModel account) {
        super(account, EnumViewEntry.BookInsertView.name());

        // create a container for showing the contents
        VBox container = new VBox(10);
        container.setPadding(new Insets(15, 5, 5, 5));
        container.setFillWidth(true);
        container.getChildren().add(createFormContent());
        container.getChildren().add(createTable());
        container.getChildren().add(createDoneButton());
        container.getChildren().add(createStatusLog("             "));

        getChildren().add(container);

        populateFields();
        myModel.subscribe(EnumEventHandler.BookSearchResult.name(), this);
        _eventHandler.register(EnumEventHandler.BookSearchResult, this::setDatatableItems);
    }

    public void setDatatableItems(Object params) {
        List<Book> books = (List<Book>) params;

        if (books != null && !books.isEmpty()) {
            _datatable.setItems(FXCollections.observableArrayList(books));
        }
    }

    private Node createDoneButton() {
        HBox hBox = new HBox(10);
        GridPane grid = new GridPane();

        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(5));
        _done.setOnAction(this::processDone);
        grid.add(_done, 0, 0);
        hBox.getChildren().add(grid);

        return hBox;
    }

    private Node createTable() {
        HBox hBox = new HBox(5);
        GridPane grid = new GridPane();

        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(5));

        _datatable.getColumns().addAll(DATATABLE_COLUMNS);

        grid.add(_datatable, 0, 0);
        hBox.getChildren().add(grid);
        return hBox;
    }

    private Node createFormContent() {
        HBox hBox = new HBox(10);
        GridPane grid = new GridPane();

        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        _submit.setOnAction(this::processSubmit);
        grid.add(new Label("Enter a title to search:"), 0, 0);
        grid.add(_titlePattern, 1, 0);
        grid.add(_submit, 2, 0);

        hBox.getChildren().add(grid);
        return hBox;
    }


    private void processSubmit(ActionEvent actionEvent) {
        String pattern = "";

        _datatable.setItems(FXCollections.emptyObservableList());
        if (!StringUtils.isNullOrEmpty(_titlePattern.getText())) {
            pattern = _titlePattern.getText();
        }
        myModel.stateChangeRequest(EnumEventHandler.BookSearchEvent.name(), pattern);
    }

    private void processDone(ActionEvent actionEvent) {
        _datatable.setItems(FXCollections.emptyObservableList());
        myModel.stateChangeRequest(EnumViewEntry.LibrarianView.name(), null);
    }


    protected MessageView createStatusLog(String initialMessage) {
        statusLog = new MessageView(initialMessage);

        return statusLog;
    }

    public void populateFields() {
        _titlePattern.setText("");
    }

    public void displayErrorMessage(String message) {
        statusLog.displayErrorMessage(message);
    }

    public void displayMessage(String message) {
        statusLog.displayMessage(message);
    }

    public void clearErrorMessage() {
        statusLog.clearErrorMessage();
    }
}