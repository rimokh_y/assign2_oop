
// specify the package
package userinterface;

// system imports

import impresario.IModel;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.WindowEvent;
import tek.wrapper.view.EnumViewEntry;

// project imports

/**
 * The class containing the Librarian View  for the ATM application
 */
//==============================================================
public class LibrarianView extends View {

    // GUI stuff
    private Button _bookInsert = new Button("INSERT NEW BOOK");
    private Button _patronInsert = new Button("INSERT NEW PATRON");
    private Button _bookSearch = new Button("SEARCH BOOKS");
    private Button _patronSearch = new Button("SEARCH PATRON");
    private Button _submitDone = new Button("DONE");


    // For showing error message
    private MessageView statusLog;

    // constructor for this class -- takes a model object
    //----------------------------------------------------------
    public LibrarianView(IModel teller) {
        super(teller, EnumViewEntry.LibrarianView.name());

        VBox container = new VBox(10);

        container.setPadding(new Insets(15, 5, 5, 5));
        container.getChildren().add(createTitle());
        container.getChildren().add(createFormContents());

        container.getChildren().add(createStatusLog("                          "));

        getChildren().add(container);
    }

    // Create the label (Text) for the title of the screen
    //-------------------------------------------------------------
    private Node createTitle() {
        Text titleText = new Text("Brockport Library System");

        titleText.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        titleText.setTextAlignment(TextAlignment.CENTER);
        titleText.setFill(Color.DARKGREEN);
        return titleText;
    }

    // Create the main form contents
    //-------------------------------------------------------------
    private GridPane createFormContents() {
        GridPane grid = new GridPane();
        VBox btnContainer = new VBox(10);

        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        _bookInsert.setOnAction(this::processBookInsert);
        _bookInsert.setPrefWidth(160);
        VBox.setVgrow(_bookInsert, Priority.ALWAYS);

        _patronInsert.setOnAction(this::processPatronInsert);
        _patronInsert.setPrefWidth(160);
        VBox.setVgrow(_patronInsert, Priority.ALWAYS);

        _bookSearch.setOnAction(this::processBookSearch);
        _bookSearch.setPrefWidth(160);
        VBox.setVgrow(_bookSearch, Priority.ALWAYS);

        _patronSearch.setOnAction(this::processPatronSearch);
        _patronSearch.setPrefWidth(160);
        VBox.setVgrow(_patronSearch, Priority.ALWAYS);

        _submitDone.setOnAction(this::processDone);
        VBox.setMargin(_submitDone, new Insets(15));
        VBox.setVgrow(_submitDone, Priority.ALWAYS);

        btnContainer.setAlignment(Pos.CENTER);
        btnContainer.getChildren().addAll(_bookInsert, _patronInsert, _bookSearch, _patronSearch, _submitDone);
        grid.add(btnContainer, 1, 3);

        return grid;
    }

    private void processBookInsert(ActionEvent actionEvent) {
        myModel.stateChangeRequest(EnumViewEntry.BookInsertView.name(), null);
    }

    private void processBookSearch(ActionEvent actionEvent) {
        myModel.stateChangeRequest(EnumViewEntry.BookSearchView.name(), null);
    }


    private void processPatronInsert(ActionEvent actionEvent) {
        myModel.stateChangeRequest(EnumViewEntry.PatronInsertView.name(), null);
    }

    private void processPatronSearch(ActionEvent actionEvent) {
        myModel.stateChangeRequest(EnumViewEntry.PatronSearchView.name(), null);
    }

    private void processDone(ActionEvent actionEvent) {
        MainStageContainer.getInstance().getOnCloseRequest()
                .handle(new WindowEvent(
                                MainStageContainer.getInstance(),
                                WindowEvent.WINDOW_CLOSE_REQUEST
                        ));
    }


    // Create the status log field
    //-------------------------------------------------------------
    private MessageView createStatusLog(String initialMessage) {

        statusLog = new MessageView(initialMessage);

        return statusLog;
    }

    //---------------------------------------------------------
    public void updateState(String key, Object value) {
    }

    /**
     * Display error message
     */
    //----------------------------------------------------------
    public void displayErrorMessage(String message) {
        statusLog.displayErrorMessage(message);
    }

    /**
     * Clear error message
     */
    //----------------------------------------------------------
    public void clearErrorMessage() {
        statusLog.clearErrorMessage();
    }

}

