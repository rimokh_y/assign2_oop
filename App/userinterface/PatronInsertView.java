// specify the package
package userinterface;

// system imports

import common.CustomProperties;
import common.StringUtils;
import impresario.IModel;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import tek.wrapper.view.EnumViewEntry;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

import static tek.wrapper.event.EnumEventHandler.PatronInsertEvent;
import static tek.wrapper.event.EnumEventHandler.UpdateStatusMessage;

// project imports

/**
 * The class containing the Account View  for the ATM application
 */
//==============================================================
public class PatronInsertView extends View {
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private TextField _name = new TextField();
    private TextField _address = new TextField();
    private TextField _city = new TextField();
    private TextField _stateCode = new TextField();
    private TextField _zip = new TextField();
    private TextField _email = new TextField();
    private TextField _dateOfBirth = new TextField();

    private Button _done = new Button("DONE");
    private Button _submit = new Button("SUBMIT");

    // For showing error message
    private MessageView statusLog;

    //----------------------------------------------------------
    public PatronInsertView(IModel account) {
        super(account, EnumViewEntry.BookInsertView.name());

        VBox container = new VBox(10);
        container.setPadding(new Insets(15, 5, 5, 5));
        container.getChildren().add(createTitle());
        container.getChildren().add(createFormContent());
        container.getChildren().add(createStatusLog("             "));
        getChildren().add(container);

        populateFields();
        myModel.subscribe(UpdateStatusMessage.name(), this);
        _eventHandler.register(UpdateStatusMessage, this::setStatusMsg);
    }


    public void setStatusMsg(Object message) {
        clearErrorMessage();
        String msg = (String) message;

        System.out.println("[PatronInsertView] Complete update msg: " + msg);
        if (msg.toLowerCase().contains("error")) {
            displayErrorMessage("Insertion failure");
        } else {
            displayMessage("Insertion success");
        }
    }
    // Create the title container
    //-------------------------------------------------------------
    private Node createTitle() {
        HBox container = new HBox();
        container.setAlignment(Pos.CENTER);

        Text titleText = new Text("      Insert New Patron      ");
        titleText.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        titleText.setWrappingWidth(300);
        titleText.setTextAlignment(TextAlignment.CENTER);
        titleText.setFill(Color.DARKGREEN);
        container.getChildren().add(titleText);

        return container;
    }

    // Create the main form content
    //-------------------------------------------------------------
    private VBox createFormContent() {
        VBox vbox = new VBox(10);
        GridPane grid = new GridPane();

        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        grid.add(new Label("Name:"), 0, 1);
        grid.add(_name, 1, 1);

        grid.add(new Label("Address:"), 0, 2);
        grid.add(_address, 1, 2);

        grid.add(new Label("City:"), 0, 3);
        grid.add(_city, 1, 3);

        grid.add(new Label("State code:"), 0, 4);
        grid.add(_stateCode, 1, 4);

        grid.add(new Label("Zip:"), 0, 5);
        grid.add(_zip, 1, 5);

        grid.add(new Label("Email:"), 0, 6);
        grid.add(_email, 1, 6);

        grid.add(new Label("Date of birth:"), 0, 7);
        grid.add(_dateOfBirth, 1, 7);

        _done.setOnAction(this::processDone);
        _submit.setOnAction(this::processSubmit);
        grid.add(_done, 0, 8);
        grid.add(_submit, 1, 8);
        vbox.getChildren().add(grid);

        return vbox;
    }

    private boolean validateDateOffset(LocalDate date, LocalDate min, LocalDate max) {
        return date.equals(max) || date.equals(min) ||
                (date.isAfter(min) && date.isBefore(max));
    }

    private List<String> generateErrorMessage() {
        List<String> errors = new ArrayList<>();

        if (StringUtils.isNullOrEmpty(_name.getText())) {
            errors.add("Name: Cannot be empty");
        }
        if (StringUtils.isNullOrEmpty(_address.getText())) {
            errors.add("Address: Cannot be empty");
        }
        if (StringUtils.isNullOrEmpty(_city.getText())) {
            errors.add("Name: Cannot be empty");
        }
        if (StringUtils.isNullOrEmpty(_stateCode.getText())) {
            errors.add("State code: Cannot be empty");
        }
        if (StringUtils.isNullOrEmpty(_zip.getText())) {
            errors.add("Zip: Cannot be empty");
        }
        if (StringUtils.isNullOrEmpty(_email.getText())) {
            errors.add("Email: Cannot be empty");
        }
        if (StringUtils.isNullOrEmpty(_dateOfBirth.getText())) {
            errors.add("Date of birth: Cannot be empty");
        } else {
            try {
                LocalDate date = LocalDate.parse(_dateOfBirth.getText(), formatter);
                LocalDate min = LocalDate.parse("1917-01-01", formatter);
                LocalDate max = LocalDate.parse("2000-01-01", formatter);

                if (!validateDateOffset(date, min, max)) {
                    errors.add("Date of birth: Patron has to be 18");
                }
            } catch (DateTimeParseException e) {
                errors.add("Date of birth: Invalid date format");
            }
        }

        return errors;
    }

    private void processSubmit(ActionEvent actionEvent) {
        clearErrorMessage();
        List<String> errors = generateErrorMessage();

        if (errors.isEmpty()) {
            myModel.stateChangeRequest(PatronInsertEvent.name(), new CustomProperties()
                    .setProperty("name", _name.getText())
                    .setProperty("address", _address.getText())
                    .setProperty("city", _city.getText())
                    .setProperty("stateCode", _stateCode.getText())
                    .setProperty("zip", _zip.getText())
                    .setProperty("email", _email.getText())
                    .setProperty("dateOfBirth", _dateOfBirth.getText())
            );
        } else {
            displayErrorMessage(errors.get(0));
        }
    }

    private void processDone(ActionEvent actionEvent) {
        myModel.stateChangeRequest(EnumViewEntry.LibrarianView.name(), null);
    }

    private MessageView createStatusLog(String initialMessage) {
        statusLog = new MessageView(initialMessage);

        return statusLog;
    }

    private void populateFields() {
        _name.setText("");
        _address.setText("");
        _city.setText("");
        _stateCode.setText("");
        _zip.setText("");
        _email.setText("");
        _dateOfBirth.setText("");
        clearErrorMessage();
    }

    private void displayErrorMessage(String message) {
        statusLog.displayErrorMessage(message);
    }

    private void displayMessage(String message) {
        statusLog.displayMessage(message);
    }

    private void clearErrorMessage() {
        statusLog.clearErrorMessage();
    }

}
