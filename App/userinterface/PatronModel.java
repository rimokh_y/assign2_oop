package userinterface;

import javafx.beans.property.SimpleStringProperty;

import java.util.Properties;
import java.util.Vector;

public class PatronModel {
    private final SimpleStringProperty idPatron;
    private final SimpleStringProperty name;
    private final SimpleStringProperty address;
    private final SimpleStringProperty city;
    private final SimpleStringProperty stateCode;
    private final SimpleStringProperty zip;
    private final SimpleStringProperty email;
    private final SimpleStringProperty dateOfBirth;

    public PatronModel(Properties patron) {
        this.idPatron = new SimpleStringProperty(patron.getProperty("idPatron"));
        this.name = new SimpleStringProperty(patron.getProperty("name"));
        this.address = new SimpleStringProperty(patron.getProperty("address"));
        this.city = new SimpleStringProperty(patron.getProperty("city"));
        this.stateCode = new SimpleStringProperty(patron.getProperty("stateCode"));
        this.zip = new SimpleStringProperty(patron.getProperty("zip"));
        this.email = new SimpleStringProperty(patron.getProperty("email"));
        this.dateOfBirth = new SimpleStringProperty(patron.getProperty("dateOfBirth"));
    }

    public String getIdPatron() {
        return idPatron.get();
    }

    public SimpleStringProperty idPatronProperty() {
        return idPatron;
    }

    public void setIdPatron(String idPatron) {
        this.idPatron.set(idPatron);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getAddress() {
        return address.get();
    }

    public SimpleStringProperty addressProperty() {
        return address;
    }

    public void setAddress(String address) {
        this.address.set(address);
    }

    public String getCity() {
        return city.get();
    }

    public SimpleStringProperty cityProperty() {
        return city;
    }

    public void setCity(String city) {
        this.city.set(city);
    }

    public String getStateCode() {
        return stateCode.get();
    }

    public SimpleStringProperty stateCodeProperty() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode.set(stateCode);
    }

    public String getZip() {
        return zip.get();
    }

    public SimpleStringProperty zipProperty() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip.set(zip);
    }

    public String getEmail() {
        return email.get();
    }

    public SimpleStringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    public String getDateOfBirth() {
        return dateOfBirth.get();
    }

    public SimpleStringProperty dateOfBirthProperty() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth.set(dateOfBirth);
    }
}
