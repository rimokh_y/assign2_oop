// specify the package
package userinterface;

// system imports

import common.StringUtils;
import impresario.IModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import model.Patron;
import tek.wrapper.view.EnumViewEntry;

import java.util.List;

import static tek.wrapper.event.EnumEventHandler.PatronSearchEvent;
import static tek.wrapper.event.EnumEventHandler.PatronSearchResult;

// project imports

/**
 * The class containing the Account View  for the ATM application
 */
//==============================================================
public class PatronSearchView extends View {
    private static ObservableList<TableColumn> DATATABLE_COLUMNS = FXCollections.observableArrayList(
            new TableColumn<>("IdPatron"),
            new TableColumn<>("Name"),
            new TableColumn<>("Address"),
            new TableColumn<>("City"),
            new TableColumn<>("State Code"),
            new TableColumn<>("Zip"),
            new TableColumn<>("Email"),
            new TableColumn<>("Date of birth")
    );

    private TableView _datatable = new TableView();
    private TextField _zip = new TextField();

    private Button _done = new Button("DONE");
    private Button _submit = new Button("SUBMIT");

    private MessageView statusLog;

    static {
        DATATABLE_COLUMNS.forEach(tableColumn -> {
            String prop = tableColumn.getText().substring(0, 1).toLowerCase() + tableColumn.getText().substring(1);

            switch (tableColumn.getText()) {
                case "PatronId":
                    prop = "patronId";
                    break;
                case "Name":
                    prop = "name";
                    tableColumn.setPrefWidth(150);
                    break;
                case "Address":
                    prop = "address";
                    tableColumn.setPrefWidth(150);
                    break;
                case "City":
                    prop = "city";
                    tableColumn.setPrefWidth(150);
                    break;
                case "Zip":
                    prop = "zip";
                    tableColumn.setPrefWidth(150);
                    break;
                case "Email":
                    tableColumn.setPrefWidth(150);
                    prop = "email";
                    break;
                case "Date of birth":
                    tableColumn.setPrefWidth(150);
                    prop = "dateOfBirth";
                    break;
            }
            tableColumn.setCellValueFactory(new PropertyValueFactory<>(prop));
        });
    }

    public PatronSearchView(IModel account) {
        super(account, EnumViewEntry.BookInsertView.name());

        // create a container for showing the contents
        VBox container = new VBox(10);
        container.setPadding(new Insets(15, 5, 5, 5));
        container.setFillWidth(true);
        container.getChildren().add(createFormContent());
        container.getChildren().add(createTable());
        container.getChildren().add(createDoneButton());
        container.getChildren().add(createStatusLog("             "));

        getChildren().add(container);

        populateFields();
        myModel.subscribe(PatronSearchResult.name(), this);
        _eventHandler.register(PatronSearchResult, this::setDatatableItems);
    }

    public void setDatatableItems(Object items) {
        clearErrorMessage();
        List<Patron> patrons = (List<Patron>) items;

        if (patrons != null && !patrons.isEmpty()) {
            _datatable.setItems(FXCollections.observableArrayList(patrons));
        }
    }
    private Node createDoneButton() {
        HBox hBox = new HBox(10);
        GridPane grid = new GridPane();

        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(5));
        _done.setOnAction(this::processDone);
        grid.add(_done, 0, 0);
        hBox.getChildren().add(grid);

        return hBox;
    }

    private Node createTable() {
        HBox hBox = new HBox(5);
        GridPane grid = new GridPane();

        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(5));

        _datatable.getColumns().addAll(DATATABLE_COLUMNS);

        grid.add(_datatable, 0, 0);
        hBox.getChildren().add(grid);
        return hBox;
    }

    private Node createFormContent() {
        HBox hBox = new HBox(10);
        GridPane grid = new GridPane();

        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        _submit.setOnAction(this::processSubmit);
        grid.add(new Label("Enter a zip to search:"), 0, 0);
        grid.add(_zip, 1, 0);
        grid.add(_submit, 2, 0);

        hBox.getChildren().add(grid);
        return hBox;
    }


    private void processSubmit(ActionEvent actionEvent) {
        String pattern = "";

        _datatable.setItems(FXCollections.emptyObservableList());
        if (!StringUtils.isNullOrEmpty(_zip.getText())) {
            pattern = _zip.getText();
        }
        myModel.stateChangeRequest(PatronSearchEvent.name(), pattern);
    }

    private void processDone(ActionEvent actionEvent) {
        _datatable.setItems(FXCollections.emptyObservableList());
        myModel.stateChangeRequest(EnumViewEntry.LibrarianView.name(), null);
    }


    protected MessageView createStatusLog(String initialMessage) {
        statusLog = new MessageView(initialMessage);

        return statusLog;
    }

    public void populateFields() {
        _zip.setText("");
    }

    public void displayErrorMessage(String message) {
        statusLog.displayErrorMessage(message);
    }

    public void displayMessage(String message) {
        statusLog.displayMessage(message);
    }

    public void clearErrorMessage() {
        statusLog.clearErrorMessage();
    }
}