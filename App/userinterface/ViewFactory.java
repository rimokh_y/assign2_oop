package userinterface;

import impresario.IModel;
import tek.wrapper.view.EnumViewEntry;

//==============================================================================
public class ViewFactory {

    public static View createView(String viewName, IModel model) {
        return EnumViewEntry.valueOf(viewName).view(model);
    }
}
