CREATE DATABASE IF NOT EXISTS java_oop;

DROP TABLE IF EXISTS Book;
DROP TABLE IF EXISTS Patron;
DROP TABLE IF EXISTS Transaction;

CREATE TABLE IF NOT EXISTS Book (
  bookId  INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  author  VARCHAR(30),
  title   VARCHAR(50),
  pubYear CHAR(4)    ,
  status  VARCHAR(10)
);

CREATE TABLE IF NOT EXISTS Patron (
  patronId    INT PRIMARY KEY AUTO_INCREMENT,
  name        VARCHAR(30),
  address     VARCHAR(50),
  city        VARCHAR(20),
  stateCode   CHAR(2)    ,
  zip         CHAR(5)    ,
  email       VARCHAR(30),
  dateOfBirth CHAR(12)   ,
  status      VARCHAR(10)
);

CREATE TABLE IF NOT EXISTS Transaction (
  transId     INT PRIMARY KEY AUTO_INCREMENT,
  bookId      INT         NOT NULL,
  patronId    INT         NOT NULL,
  transType   VARCHAR(10) NOT NULL,
  dateOfTrans VARCHAR(12) NOT NULL,
  FOREIGN KEY (patronId) REFERENCES Patron (patronId),
  FOREIGN KEY (bookId) REFERENCES Book (bookId)
);
